React Native module for Tapjoy - only works for Android now (and not fully yet - see Notes in the bottom)

# react-native-tapjoy-android

## Getting started

`$ npm install react-native-tapjoy-android --save`

### Mostly automatic installation

`$ react-native link react-native-tapjoy-android`

<p>Also some manual steps need to be followed, as explained here: <a href='https://dev.tapjoy.com/sdk-integration/android/getting-started-guide-publishers-android/'>https://dev.tapjoy.com/sdk-integration/android/getting-started-guide-publishers-android/</a>. In short, for Android (currently the only supported), edit your AndroidManifest.xml file and add (taken from the linked document on Jul 17 2019):</p>
<pre>
	&lt;manifest ...>
  ...
		&lt;uses-permission android:name="android.permission.INTERNET"/>
		&lt;uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
		&lt;uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
  ...
	&lt;/manifest>
  ...
	&lt;activity
		android:name="com.tapjoy.TJAdUnitActivity"
		android:configChanges="orientation|keyboardHidden|screenSize"
		android:hardwareAccelerated="true"
		android:theme="@android:style/Theme.Translucent.NoTitleBar.Fullscreen" />
	&lt;activity
		android:name="com.tapjoy.TJContentActivity"
		android:configChanges="orientation|keyboardHidden|screenSize"
		android:theme="@android:style/Theme.Translucent.NoTitleBar"
		android:hardwareAccelerated="true" />
  ...
	&lt;meta-data
		android:name="com.google.android.gms.version"
		android:value="@integer/google_play_services_version" />
</pre>

<p>You will also need to add the same configChanges to your App’s Manifest activity:</p>

<pre>
	android:configChanges="orientation|keyboardHidden|screenSize"
</pre>

### Manual installation


#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.coldevel.rntapjoyandroid.RNTapjoyAndroidPackage;` to the imports at the top of the file
  - Add `new RNTapjoyAndroidPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-tapjoy-android'
  	project(':react-native-tapjoy-android').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-tapjoy-android/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-tapjoy-android')
  	```


## Usage
```javascript

import TapjoySDK from 'react-native-tapjoy-android';

// Somewhere in app.js / index.js at the top of the file
TapjoySDK.initialize(
	// See https://dev.tapjoy.com/sdk-integration/android/getting-started-guide-publishers-android/ to locate your SDK key
	TAPJOY_SDK_KEY,
	() => console.log('Initialization success'),
	() => console.log('Initialization failure'),
);


// When you want to load an ad (probably somewhere early, too, but after initialization succeeded above
TapjoySDK.requestPlacement(
	placementName,
	() => console.log('Ad ready to show'),
	() => console.log('Ad not ready to show'),
);

// When you want to show an ad
TapjoySDK.showPlacement(
	placementName,
	() => console.error('Error showing Ad'),
	() => console.log('Ad shown'),
	() => console.log('Ad dismissed'),
);
```

## Notes
<ul>
<li>Even though I followed all instructions and created the library as above, I am still getting in LogCat:
<i>2019-06-30 22:46:28.561 19352-19656/com.mauking3 I/TapjoyLog:TJCorePlacement: Content request delivered successfully for placement AppLaunch, contentAvailable: false, mediationAgent: null</i><br/>
<b>Update</b>: I was getting this error apparently because in my project a wrong type of content cards was used - instead of an traditional content cards, I was using programmatic content cards. Once that is fixed, the above error goes away. 
</li>
<li>
	Still getting crashes when trying to show an ad (after successfully having connected and acquired the placement). Getting errors in the logcat like this:<br/>
1983-2001/system_process E/ActivityManager: Failed to schedule configuration change (android.os.DeadObjectException)<br/>
1983-2128/system_process E/TaskPersister: File error accessing recents directory (directory doesn't exist?).
2019-07-18 02:00:12.667 3131-3214/com.google.android.googlequicksearchbox:search E/EntrySyncManager: Cannot determine account name: drop request
2019-07-18 02:00:12.683 3131-3214/com.google.android.googlequicksearchbox:search E/NowController: Failed to access data from EntryProvider. ExecutionException.
    java.util.concurrent.ExecutionException: com.google.android.apps.gsa.sidekick.main.h.n: Could not complete scheduled request to refresh entries. ClientErrorCode: 3<br/>
3131-3410/com.google.android.googlequicksearchbox:search E/PlaceStateUpdater: Received no places
2019-07-18 02:00:15.402 2129-3361/com.google.android.gms.persistent E/Volley: [72] BasicNetwork.performRequest: Unexpected response code 400 for https://www.googleapis.com/placesandroid/v1/getPlaceById?key=AIzaSyAf4nrRiEKvqzlRKTncQaAXMzb3ePYHr8Y
2019-07-18 02:00:15.476 2129-6031/com.google.android.gms.persistent E/AsyncOperation: serviceID=65, operation=GetPlaceById
    OperationException[Status{statusCode=ERROR, resolution=null}]
        at bear.b(:com.google.android.gms@16089022@16.0.89 (040700-239467275):1)
        at beak.a(:com.google.android.gms@16089022@16.0.89 (040700-239467275):28)
        at zgb.run(:com.google.android.gms@16089022@16.0.89 (040700-239467275):27)
        at bgot.run(:com.google.android.gms@16089022@16.0.89 (040700-239467275):2)
        at rrt.b(:com.google.android.gms@16089022@16.0.89 (040700-239467275):32)
        at rrt.run(:com.google.android.gms@16089022@16.0.89 (040700-239467275):21)
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1167)
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:641)
        at rxx.run(Unknown Source:7)
        at java.lang.Thread.run(Thread.java:764)
2019-07-18 02:00:18.509 1656-1656/? E/AudioPolicyIntefaceImpl: getInputForAttr permission denied: recording not allowed for uid 10039 pid 3131
2019-07-18 02:00:18.510 1656-1656/? E/AudioFlinger: createRecord() checkRecordThread_l failed<br/>
(and so on)
</li>
</ul>




