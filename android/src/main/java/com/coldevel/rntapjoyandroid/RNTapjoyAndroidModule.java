
package com.coldevel.rntapjoyandroid;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import android.content.Context;
import android.app.Activity;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyConnectFlag;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJConnectListener;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJError;
import com.tapjoy.TJActionRequest;
import java.util.Hashtable;

class InitializationConnectListener implements TJConnectListener {
    Activity m_currentActivity;
    private Callback m_onSuccess, m_onFailure;

    InitializationConnectListener(Activity activity, Callback onSuccess, Callback onFailure) {
        m_currentActivity = activity;
        m_onSuccess = onSuccess;
        m_onFailure = onFailure;
    }

    @Override
    public void onConnectSuccess() {
        Tapjoy.setActivity(m_currentActivity);
        this.m_onSuccess.invoke();
    }

    @Override
    public void onConnectFailure() {
        this.m_onFailure.invoke();
    }
}

class PlacementListener implements TJPlacementListener {
    private Callback m_onReady, m_onCannotLoad, m_onDismissed, m_onShown;
    PlacementListener(
            Callback onReady,
            Callback onCannotLoad
    ) {
        this.m_onReady = onReady;
        this.m_onCannotLoad = onCannotLoad;
        this.m_onShown = null;
        this.m_onDismissed = null;
    }

    public void addShowListeners(Callback onShown, Callback onDismissed) {
        this.m_onShown = onShown;
        this.m_onDismissed = onDismissed;
    }

    @Override public void onClick(TJPlacement placement) {}
    @Override public void onContentDismiss(TJPlacement placement) {
        if (this.m_onDismissed != null) {
            this.m_onDismissed.invoke();
        }
    }
    @Override public void onContentReady(TJPlacement placement) {
        this.m_onReady.invoke();
    }
    @Override public void onContentShow(TJPlacement placement) {
        if (this.m_onShown != null) {
            this.m_onShown.invoke();
        }
    }
    @Override public void onPurchaseRequest(TJPlacement placement, TJActionRequest request, java.lang.String productId) {}
    @Override public void onRequestFailure(TJPlacement placement, TJError error) {
        this.m_onCannotLoad.invoke();
    }
    @Override public void onRequestSuccess(TJPlacement placement) {}
    @Override public void onRewardRequest(TJPlacement placement, TJActionRequest request, java.lang.String itemId, int quantity) {}
}

public class RNTapjoyAndroidModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private Hashtable<String, PlacementListener> m_placementListeners;
    private Hashtable<String, TJPlacement> m_placements;

    public RNTapjoyAndroidModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        this.m_placementListeners = new Hashtable<>();
        this.m_placements = new Hashtable<>();
    }

    @Override
    public String getName() {
        return "RNTapjoyAndroid";
    }

    @ReactMethod
    public void initialize(String tapjoySDKKey, Callback onSuccess, Callback onFailure) {
        Hashtable<String, Object> connectFlags = new Hashtable<>();
        connectFlags.put(TapjoyConnectFlag.ENABLE_LOGGING, "true");      // remember to turn this off for your production builds!

        Activity currentActivity = getCurrentActivity();
        Tapjoy.connect(
                currentActivity,
                tapjoySDKKey,
                connectFlags,
                new InitializationConnectListener(currentActivity, onSuccess, onFailure)
        );
    }

    @ReactMethod
    public void requestPlacement(String placementName, Callback onReady, Callback onCannotLoad) {
        PlacementListener listener = new PlacementListener(onReady, onCannotLoad);
        this.m_placementListeners.put(placementName, listener);

        TJPlacement placement = Tapjoy.getPlacement(placementName, listener);
        this.m_placements.put(placementName, placement);
        placement.requestContent();
    }

    @ReactMethod
    public void showPlacement(String placementName, Callback onError, Callback onShown, Callback onDismissed) {
        PlacementListener listener = m_placementListeners.get(placementName);
        if (listener == null) {
            onError.invoke("Listener not found - probably requestPlacement() was not called first");
            return;
        }
        TJPlacement placement = m_placements.get(placementName);
        if (placement == null) {
            onError.invoke("Placement not found - probably requestPlacement() was not called first");
            return;
        }
        if (!Tapjoy.isConnected()) {
            onError.invoke("Tapjoy is not connected - probably initialize() was not called first");
            return;
        }
        listener.addShowListeners(onShown, onDismissed);
        placement.requestContent();
    }
}